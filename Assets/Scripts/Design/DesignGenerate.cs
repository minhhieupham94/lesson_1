using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Generate", menuName = "Resource/Generate")]
public class DesignGenerate : ScriptableObject
{
    public float Velocity;
    public int Width;
    public int Height;
    public int LimitTypeCandy;
}
