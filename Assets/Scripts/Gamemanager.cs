using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager Instance;
    private int width;
    private int height;
    private int totalCandy;
    private int countCandy;
    private int destroyCandyAmount;
    private int limitTypeCandy;
    private readonly List<Vector2Int> ListDirectionToCheck = new List<Vector2Int>() { Vector2Int.left, Vector2Int.right, Vector2Int.up, Vector2Int.down };
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        width = ResourceManager.Current.GetDesignGenerate().Width;
        height = ResourceManager.Current.GetDesignGenerate().Height;
        totalCandy = width * height;
        Initialized();
        LoadResource();
    }

    private void Initialized()
    {
        Boardmanager.Instance.Initialized();
        CandyManager.Instance.Initialized(width, height);
        DispatchPlayerEnable(false);
    }

    private void LoadResource()
    {
        Boardmanager.Instance.LoadBoard(width, height);
        limitTypeCandy = ResourceManager.Current.GetDesignGenerate().LimitTypeCandy;
        CandyManager.Instance.GenerateCandies(width * height, limitTypeCandy);
        var timeDelay = ResourceManager.Current.GetDesignGenerate().Velocity;
        CandyManager.Instance.DoShowCandy(timeDelay,EvalueAte);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CandyManager.Instance.MainFixedUpdate(Time.fixedDeltaTime);
    }
    public void EvalueAte()
    {
        StartCoroutine(EvalueAteYield());
    }

    private IEnumerator EvalueAteYield()
    {
        yield return new WaitForSeconds(0.5f);
        var scores = 0;
        var rowCheck = CheckScore(new Vector2Int(1, 0), width * height);
        var collumCheck = CheckScore(new Vector2Int(0, 1), width * height);

        //Destroy list collect
        var listCollect = new List<Candy>();
        listCollect.AddRange(rowCheck.listCandy.GetRange(0, rowCheck.listCandy.Count));
        listCollect.AddRange(collumCheck.listCandy.GetRange(0, collumCheck.listCandy.Count));
        if (listCollect.Count > 0)
        {
            // take score
            scores += collumCheck.score;
            scores += rowCheck.score;
            Debug.Log($"Check Score {scores}");

            CandyManager.Instance.DestroyCandy(listCollect, ReOrganizeListCandy);
        }
        else
        {
            if (IsWin())
            {
                DispatchPlayerEnable(false);
                //Anim win
            }
            else
            {
                CheckStuckAndPlayerEnable();
            }
        }
    }

    private SlotData preSelected;
    internal void OnSelected(SlotData data)
    {
        if(preSelected is null)
        {
            preSelected = data;
        }
        else
        {
            if(Vector2.Distance(preSelected.Coordinate,data.Coordinate) != 1)
            {
                if (preSelected != data)
                    preSelected = data;
                else
                    preSelected = null;
                return;
            }
            else
            {
                //AnimSwap
            }

            if(CheckSwap(preSelected, data) || CheckSwap(data,preSelected))
            {
                //Swap
                DispatchPlayerEnable(false);
                CandyManager.Instance.SwapCandy(preSelected.Coordinate, data.Coordinate, EvalueAte);
            }
            else
            {
                //Anim return
                preSelected = null;
            }
        }
    }

    private bool CheckSwap(SlotData selected1, SlotData selected2)
    {
        var candy1 = CandyManager.Instance.GetCandyByCoordinate(selected1.Coordinate);
        var candy2 = CandyManager.Instance.GetCandyByCoordinate(selected2.Coordinate);

        if(candy1 is null || candy2 is null || candy1.GetType() == candy2.GetType())
        {
            return false;
        }

        foreach (var dir in ListDirectionToCheck)
        {
            if (selected1.Coordinate + dir == selected2.Coordinate)
            {
                continue;
            }

            var coor = selected1.Coordinate + dir;
            var candy = CandyManager.Instance.GetCandyByCoordinate(coor);

            if (candy is null)
            {
                continue;
            }

            if (candy.GetType() == candy2.GetType())
            {
                coor += dir;
                candy = CandyManager.Instance.GetCandyByCoordinate(coor);
                if(candy is not null && candy.GetType() == candy2.GetType())
                {
                    return true;
                }

                coor = selected1.Coordinate - dir;

                if(coor == selected2.Coordinate)
                {
                    continue;
                }

                candy = CandyManager.Instance.GetCandyByCoordinate(coor);
                if (candy is not null && candy.GetType() == candy2.GetType())
                {
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsWin()
    {
        return CandyManager.Instance.IsWin();
    }

    private bool IsStuck()
    {
        
        Vector2Int horizontal = new Vector2Int(1, 0);
        Vector2Int vertical = new Vector2Int(0, 1);
        if(CheckStuck(horizontal, vertical) && CheckStuck(vertical, horizontal))
        {
            return true;
        }
        return false;
    }

    private bool CheckStuck(Vector2Int horizontal, Vector2Int vertical)
    {
        Vector2Int origin = Vector2Int.zero;
        Vector2Int coor1 = Vector2Int.zero;
        Vector2Int coor2 = Vector2Int.zero; 
        Vector2Int coor3 = Vector2Int.zero;
        Dictionary<Vector2Int,Candy> list = new();

        int count = 0;
        while (count < totalCandy)
        {
            if(list.Count > 0)
            {
                list.Clear();
            }
            coor2 = coor1 + horizontal;
            coor3 = coor1 + 2 * horizontal;

            Debug.Log($"CheckStuck {count} :: {coor1} | {coor2} | {coor3} ");

            var candy = CandyManager.Instance.GetCandyByCoordinate(coor1);
            var candy1 = CandyManager.Instance.GetCandyByCoordinate(coor2);
            var candy2 = CandyManager.Instance.GetCandyByCoordinate(coor3);

            if(candy is not null && candy1 is not null && candy1 is not null)
            {
                bool hasList = false;
                Vector2Int othercandy = Vector2Int.zero;
                if (candy.GetType() == candy1.GetType())
                {
                    hasList = true;
                    othercandy = coor3;
                }
                else if (candy1.GetType() == candy2.GetType())
                {
                    hasList = true;
                    othercandy = coor1;
                }
                else if (candy.GetType() == candy2.GetType())
                {
                    hasList = true;
                    othercandy = coor2;
                }
                Debug.Log($"CheckStuck hasList {hasList}");
                if (hasList)
                {
                    list.Add(coor1, candy);
                    list.Add(coor2, candy1);
                    list.Add(coor3, candy2);
                    if (CanEvalueAte(othercandy))
                    {
                        return false;
                    }
                }
            }          
            
            if(coor3.CheckHitWall(horizontal))
            {
                count += 3;
                origin += vertical;
                coor1 = origin;
            }
            else
            {
                count++;
                coor1 += horizontal;
            }
        }

        return true;

        bool CanEvalueAte(Vector2Int otherCoor)
        {
            Debug.Log($"CheckStuck CanEvalueAte");
            var otherCandy = list[otherCoor];
            list.Remove(otherCoor);
            foreach (var dir in ListDirectionToCheck)
            {
                var newCoor = otherCoor + dir;
                if (list.ContainsKey(newCoor))
                {
                    continue;
                }
                Debug.Log($"CheckStuck CanEvalueAte newCoor {newCoor}");
                var candyCheck = CandyManager.Instance.GetCandyByCoordinate(newCoor);
                if(candyCheck is not null && candyCheck.GetType() != otherCandy.GetType())
                {
                    int count = 0;
                    foreach (var candy in list.Values)
                    {
                        if(candy.GetType() == candyCheck.GetType())
                        {
                            Debug.Log($"CheckStuck CanEvalueAte count {count}");
                            count++;
                        }

                        Debug.Log($"CheckStuck CanEvalueAte count {count} == {list.Count}");
                        if (count == list.Count)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    private void CheckStuckAndPlayerEnable()
    {
        if (IsStuck())
        {
            StartCoroutine(ReGenerateCandy());
        }
        else
        {
            PlayerEnable();
        }
    }

    private IEnumerator ReGenerateCandy()
    {
        yield return new WaitForSeconds(0.5f);
        CandyManager.Instance.GenerateCandies(width * height, limitTypeCandy,false);
        var timeDelay = ResourceManager.Current.GetDesignGenerate().Velocity;
        CandyManager.Instance.DoShowCandy(timeDelay, EvalueAte);
    }

    private void PlayerEnable()
    {
        DispatchPlayerEnable(true);
    }

    private Action<bool> playerEnableAction;

    public void PlayerEnableRegister(Action<bool> act)
    {
        if(act != null)
        {
            playerEnableAction += act;
        }
    }

    public void PlayerEnableUnRegister(Action<bool> act)
    {
        if (playerEnableAction != null)
        {
            playerEnableAction -= act;
        }
    }

    public void DispatchPlayerEnable(bool _playerEnable)

    {
        playerEnableAction?.Invoke(_playerEnable);
    }

    private void ReOrganizeListCandy()
    {
        StartCoroutine(ReOrganizeListCandyYield());
    }

    private IEnumerator ReOrganizeListCandyYield()
    {
        yield return new WaitForSeconds(0.5f);
        CandyManager.Instance.ReOrganize();
    }

    public (List<Candy> listCandy,int score) CheckScore(Vector2Int direction, int amount)
    {
        int scores = 0;
        int count = 0;
        int pretype = -1;
        var res = new List<Candy>();
        var listCollect = new List<Candy>();
        Vector2Int origin = Vector2Int.zero;
        Vector2Int curPosition = origin;
        var candyManager = CandyManager.Instance;
        while (count < amount)
        {
            var candy = candyManager.GetCandyByCoordinate(curPosition);
            var curType = candy.GetType();
            //Debug.Log($"Check Coordinate {curPosition} :: curType {curType}:: HitWall {curPosition.CheckHitWall(direction)}");
            //if head row, add list collect
            if (pretype == -1)
            {
                listCollect.Add(candy);
                pretype = curType;
                curPosition += direction;
            }

            // if not hit wall,
            //if type = pretype, add collection
            // if type != pretype, check evalue
            else if(!curPosition.CheckHitWall(direction) && pretype != -1)
            {
                if (curType != pretype)
                {
                    if(listCollect.Count >= 3)
                    {
                        res.AddRange(listCollect.GetRange(0, listCollect.Count));
                        foreach (var c in listCollect)
                        {
                            c.SetEmpty();
                        }
                        scores += ResourceManager.Current.GetScore(pretype, listCollect.Count);
                    }
                    listCollect.Clear();
                    pretype = curType;                   
                }
                listCollect.Add(candy);
                curPosition += direction;
            }

            // if hit wall
            // check evalue
            // Add new collum
            else if(curPosition.CheckHitWall(direction) && pretype != -1)
            {
                if (curType == pretype)
                {
                    listCollect.Add(candy);
                }

                if (listCollect.Count >= 3)
                {
                    res.AddRange(listCollect.GetRange(0, listCollect.Count));
                    foreach (var c in listCollect)
                    {
                        c.SetEmpty();
                    }
                    scores += ResourceManager.Current.GetScore(pretype, listCollect.Count);
                }
                listCollect.Clear();
                pretype = -1;
                origin += new Vector2Int(direction.y, direction.x);
                curPosition = origin;
                
            }
            count++;
        }
        return (res, scores);
    }

}
