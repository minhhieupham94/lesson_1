using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class CustomEditor : MonoBehaviour
{
    public static void OpenScene(string sceneName)
    {
        // ask save or don't save scene in Hierarchy before open new scene
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/Scenes/" + sceneName + ".unity");
        }
    }

    [MenuItem("Lesson/Load GameScene &1")]
    public static void LoadGameScene()
    {
        OpenScene("GameScene");
    }

    [MenuItem("Lesson/Run &1")]
    public static void Run()
    {
        OpenScene("GameScene");
        EditorApplication.isPlaying = true;
    }
}
