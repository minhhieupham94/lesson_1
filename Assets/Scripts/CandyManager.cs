using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

public class CandyManager : MonoBehaviour
{
    public static CandyManager Instance;
    public RectTransform CandyContain;
    public Candy CandyPrefab;
    private Dictionary<int, Candy> candyDiction = new Dictionary<int, Candy>();
    private List<Candy> candyList;
    private List<Vector2> passage;
    private int totalCandy;
    private List<Vector2Int> spiralCoordinateList;
    private int width, height;
    private int destroyCandyCounter;
    private int destroyCandyAmount;
    private int countCandy;
    private Action onHideCompleteCallback;
    private Action onShowComleteCallBack;
    private bool showAnim;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Initialized(int width, int height)
    {
        candyList = new();
        passage = new List<Vector2>();
        this.width = width;
        this.height = height;
        totalCandy = width * height;
        var boardContain = Boardmanager.Instance.BoardContain.rect;
        CandyContain.rect.Set(boardContain.x,boardContain.y, boardContain.width, boardContain.height);
    }

    /// <summary>
    /// Generate Candy by amount, type candy by 0 -> limit
    /// </summary>
    public void GenerateCandies(int number = 1, int limit = 9, bool hasKey = true)
    {
        passage = ResourceManager.Current.GetPassage();
        var velocity = ResourceManager.Current.GetDesignGenerate().Velocity;
        for (int i = 0; i < number; i++)
        {
            RandomCandy(i, velocity, limit, hasKey);
        }
    }

    private void RandomCandy(int Id,float velocity, int limit = 9, bool hasKey = true)
    {
        int type = 0;
        if (Id == 0 && hasKey)
        {
            type = 0;
        }
        else
        {
            type = UnityEngine.Random.Range(1, limit);
        }
        Candy candy = null;
        if (!candyDiction.ContainsKey(Id))
        {
            candy = Instantiate(CandyPrefab, CandyContain, false);
            candyDiction[Id] = candy;
            candyList.Add(candy);
        }
        else
        {
            candy = candyDiction[Id];
        }

        if(candy.GetType() != 0)
        {
            var data = new CandyData()
            {
                Id = Id,
                Velocity = velocity,
                Type = type,
                Passage = passage,
            };
            candy.Initialized(data);
        }
        candy.SetSpiral(0);
        candy.transform.position = passage[0];
        candy.gameObject.SetSafeActive(false);
    }

    internal void ReOrganize()
    {
        var limitTypeCandy = ResourceManager.Current.GetDesignGenerate().LimitTypeCandy;
        var velocity = ResourceManager.Current.GetDesignGenerate().Velocity;
        for (int i = totalCandy - 1; i >= 0; i--)
        {
            if(candyDiction[i].IsEmty)
            {
                if(i > 0)
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (!candyDiction[j].IsEmty)
                        {
                            SwapCandy(i, j);
                            break;
                        }
                    }
                }
            }
        }

        for(int i = 0; i < totalCandy;i ++)
        {
            if(candyDiction[i].IsEmty)
            {
                RandomCandy(i, velocity, limitTypeCandy,false);
            }
        }

        DoShowCandy(0,null);

        foreach (var item in candyDiction)
        {
            Debug.Log($"candyDiction1 {item.Key} || {item.Value.GetSpiral()} || {item.Value.GetData().GetId()}");
        }
    }

    private void SwapCandy(int _candyID1, int _candyID2)
    {
        Debug.Log($"SwapCandy_1    {candyDiction[_candyID1].GetData().GetId()} | {candyDiction[_candyID2].GetData().GetId()}  :::  {_candyID1} | {_candyID2}");
        var t = candyDiction[_candyID1].GetData().GetId();
        candyDiction[_candyID1].GetData().SetId(candyDiction[_candyID2].GetData().GetId());
        candyDiction[_candyID2].GetData().SetId(t);

        (candyDiction[_candyID1], candyDiction[_candyID2]) = (candyDiction[_candyID2], candyDiction[_candyID1]);
        Debug.Log($"SwapCandy_2    {candyDiction[_candyID1].GetData().GetId()} | {candyDiction[_candyID2].GetData().GetId()}  :::  {_candyID1} | {_candyID2}");

    }

    public void DoShowCandy(float timeDelay, Action callback)
    {
        showAnim = true;
        StartCoroutine(ShowCandyYield(OnShowComplete, timeDelay));
        if(callback != null)
        {
            onShowComleteCallBack = callback;
        }
    }

    private IEnumerator ShowCandyYield(Action callback, float _timedelay)
    {
        if (candyDiction != null)
        {
            for (int i = candyDiction.Count - 1; i >= 0;i--)
            {
                yield return new WaitForSeconds(_timedelay);
               // Debug.Log($"time Spawn {Time.time}");
                candyDiction[i].DoShow(callback); 
            }
        }
        yield return new WaitForEndOfFrame();
    }

    public void MainFixedUpdate(float time)
    {
        if(showAnim)
        {
            foreach (var candy in candyList)
            {
                candy.MainFixedUpdate(time);
            }
        }
    }

    public Candy GetCandyByCoordinate(Vector2Int coor)
    {
        var id = coor.ConvertCoordinateToIdCandy();
        if (candyDiction.ContainsKey(id))
        {
            return candyDiction[id];
        }
        return null;
    }
    public Candy GetCandyById(int id)
    {
        if (candyDiction.ContainsKey(id))
        {
            return candyDiction[id];
        }
        return null;
    }
    public void DestroyCandy(List<Candy> list, Action complete)
    {
        if(list is not null && list.Count > 0)
        {
            onHideCompleteCallback = complete;
            StartCoroutine(DestroyListCandyYield(list));
        }
    }
    private IEnumerator DestroyListCandyYield(List<Candy> list)
    {
        Debug.Log($"DestroyListCandy {list.Count}");
        if (list is not null && list.Count > 0)
        {
            yield return new WaitForSecondsRealtime(1f);
            destroyCandyCounter = 0;
            destroyCandyAmount = list.Count;
            showAnim = true;
            foreach (var candy in list)
            {
                candy.SetEmpty();
                if(!candy.IsDestroy)
                {
                    candy.DoHide(DestroyComplete);
                }
                
            }
        }
    }

    public void DestroyComplete()
    {
        destroyCandyCounter++;
        if(destroyCandyCounter >= destroyCandyAmount)
        {
            OnhideComplete();
            destroyCandyCounter = 0;
        }
    }
    public void SwapCandy(Vector2Int coordinate1, Vector2Int coordinate2, Action onComplete)
    {
        var id1 = coordinate1.ConvertCoordinateToIdCandy();
        var id2 = coordinate2.ConvertCoordinateToIdCandy();
        
        ShowAnimSwap(coordinate1, coordinate2,() => {
            SwapCandy(id1, id2);
            onComplete?.Invoke();
        });
        
    }

    private void ShowAnimSwap(Vector2Int coordinate1, Vector2Int coordinate2, Action onComplete)
    {
        showAnim = true;
        var candy1 = GetCandyByCoordinate(coordinate1);
        var candy2 = GetCandyByCoordinate(coordinate2);

        var target1 = candy2.transform.position;
        var target2 = candy1.transform.position;
        candy1.DoSwap(target1,null);
        candy2.DoSwap(target2, () => { 
            onComplete();
            showAnim = false;
        });
    }
    public void OnhideComplete()
    {
        showAnim = false;
        onHideCompleteCallback?.Invoke();
    }

    public void OnShowComplete()
    {
        countCandy++;
        if(countCandy >= totalCandy)
        {
            showAnim = false;
            onShowComleteCallBack?.Invoke();
            countCandy = 0;

            foreach (var item in candyDiction)
            {
                Debug.Log($"candyDiction2 {item.Key} || {item.Value.GetSpiral()} || {item.Value.GetData().GetId()}");
            }
        }
    }

    public bool IsWin()
    {
        return candyDiction[totalCandy - 1].GetType() == 0;
    }
}
