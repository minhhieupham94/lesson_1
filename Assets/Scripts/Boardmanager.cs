using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boardmanager : MonoBehaviour
{
    public static Boardmanager Instance;
    public Slot SlotPrefab;
    public RectTransform BoardContain;
    public float SlotWidth;
    public float Offset;
    public float SlotHeight;
    private int width;
    private int height;
    private Dictionary<int,Slot> slotList;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Initialized()
    {
        slotList = new Dictionary<int, Slot>();
        Gamemanager.Instance.PlayerEnableRegister(OnEnableButton);
    }

    private void OnEnableButton(bool val)
    {
        if(slotList is not null)
        {
            foreach (var item in slotList.Values)
            {
                item.OnEnableButton(val);
            }
        }
    }

    // Load Board
    public void LoadBoard(int w, int h)
    {
        SetPostitionContain(w, h);

        height = h;
        width = w;
        var total = w * h;
        for (int i = 0; i < total; i++)
        {
            var slot = Instantiate(SlotPrefab, BoardContain,false);
            var coordinate = i.ConvertBoardIdToCoordinate();
            slot.Init(new SlotData(i, coordinate));
            slot.OnSelectedRegister(OnSelected);
            slot.transform.localPosition = new Vector2(coordinate.x * (SlotWidth + Offset), coordinate.y * (SlotHeight + Offset)) + new Vector2(SlotWidth / 2, SlotHeight / 2);
            slot.name = $"slot {coordinate.x}x{coordinate.y}";
            
            if(!slotList.ContainsKey(i))
            {
                slotList[i] = slot;
            }
        }
    }

    private void OnSelected(SlotData _data)
    {
        if (_data is not null)
            Gamemanager.Instance.OnSelected(_data);
    }

    private void SetPostitionContain(int w, int h)
    {
        var sizeContain = new Vector2(w * SlotWidth + (w - 1) * Offset, h * SlotHeight + (h - 1) * Offset);
        var pos = BoardContain.localPosition;
        BoardContain.localPosition = new Vector2(pos.x - sizeContain.x / 2, pos.y - sizeContain.y / 2);
        BoardContain.sizeDelta = sizeContain;
    }
   
    public Slot GetSlotById(int id)
    {
        if(slotList != null)
        {
            if(slotList.ContainsKey(id))
            {
                return slotList[id];
            }
        }
        return null;
    }
    public Slot GetSlotByCoordinate(Vector2Int coordinate)
    {
        var id = coordinate.ConvertCoordinateToIdBoard();
        return GetSlotById(id);
    }
}
