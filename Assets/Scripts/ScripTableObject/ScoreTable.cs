using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[CreateAssetMenu(fileName = "new ScoreTable", menuName = "Resource/ScoreTable")]
public class ScoreTable : ScriptableObject
{
    [Serializable]
    public class ScoringStr
    {
        public int Amount;
        public int Score;
    }

    public List<ScoringStr> ListScore;

    public int GetScore(int amount)
    {
        var Scoring = ListScore.FirstOrDefault((v) => v.Amount == amount);
        if(Scoring != null)
        {
            return Scoring.Score;
        }
        Debug.LogError($"ScoreTable not contain {amount}!!!");
        return 0;
    }
}
