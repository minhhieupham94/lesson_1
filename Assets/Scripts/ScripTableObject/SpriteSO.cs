using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SpriteResource
{
    public int Id;
    public Sprite Sprite;
}

[CreateAssetMenu(menuName = "Resource/Sprite", order = 1)]
public class SpriteSO : ScriptableObject
{
    public List<SpriteResource> Data;

    public Sprite GetSprite(int Id)
    {
        if(Data != null)
        {
            var dt = Data.FirstOrDefault((v) => v.Id == Id);
            if (dt != null)
            {
                return dt.Sprite;
            }
            Debug.LogError("Id Sprite is not exit !!!");
        }
        else
        {
            Debug.LogError("Data Sprite is null !!!");
        }
        return Data[0].Sprite;
    }
}
