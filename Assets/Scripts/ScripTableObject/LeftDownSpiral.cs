using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LeftDownSpiral", menuName = "Resource/LeftDownSpiral")]
public class LeftDownSpiral : Trajectory
{
    public override List<Vector2> GetPassage(int width, int height)
    {
        var res = new List<Vector2>();
        // get list Coordinate (x,y) of board
        var coordinateList = GetListSpiralCoordinate(width, height);

        foreach (var c in coordinateList)
        {
            // Convert list Coordinates to list World position
            var slot = Boardmanager.Instance.GetSlotByCoordinate(c);
            if (slot != null)
            {
                var pos = slot.transform.position;
                res.Add(pos);
            }
        }
        return res;
    }
    public override List<Vector2Int> GetListSpiralCoordinate(int width, int height)
    {
        var coordinateList = new List<Vector2Int>();
        int left = 0;
        int right = width - 1;
        int bottom = 0;
        int top = height - 1;

        while (left <= right && bottom <= top)
        {
            // last row
            for (int i = left; i <= right; i++)
            {
                var coordinate = new Vector2Int(i, top);
                coordinateList.Add(coordinate);
            }
            top--;

            //Last Collum
            for (int i = top; i >= bottom; i--)
            {
                var coordinate = new Vector2Int(right, i);
                coordinateList.Add(coordinate);
            }
            right--;

            // First row
            for (int i = right; i >= left; i--)
            {
                var coordinate = new Vector2Int(i, bottom);
                coordinateList.Add(coordinate);
            }
            bottom++;

            // first collum
            for (int i = bottom; i <= top; i++)
            {
                var coordinate = new Vector2Int(left, i);
                coordinateList.Add(coordinate);
            }
            left++;    
        }
        return coordinateList;
    }
}
