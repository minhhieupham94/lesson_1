using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trajectory : ScriptableObject
{
    /// <summary>
    /// Get list point, Passage of candy
    /// </summary>
    public abstract List<Vector2> GetPassage(int width,int height);
    public abstract List<Vector2Int> GetListSpiralCoordinate(int width, int height);
}
