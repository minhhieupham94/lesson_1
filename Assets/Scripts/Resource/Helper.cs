using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    private static List<Vector2Int> spiralCoordinateList;

    public static void SetSafeActive(this GameObject gameObject, bool val)
    {
        if (gameObject.activeSelf != val)
        {
            gameObject.SetActive(val);
        }
    }

    public static bool CheckHitWall(this Vector2Int curPosition, Vector2Int direction)
    {
        var rightLimit = ResourceManager.Current.GetDesignGenerate().Width -1;
        var upLimit = ResourceManager.Current.GetDesignGenerate().Height -1;
        if(direction.x != 0)
        {
            if (curPosition.x <= 0 || curPosition.x >= rightLimit)
                return true;
        }
        else if(direction.y != 0)
        {
            if (curPosition.y <= 0 || curPosition.y >= upLimit)
                return true;
        }

        return false;
    }
    public static int ConvertCoordinateToIdBoard(this Vector2Int coordinate)
    {
        var width = ResourceManager.Current.GetDesignGenerate().Width;
        return coordinate.y * width + coordinate.x;
    }
    public static Vector2Int ConvertBoardIdToCoordinate(this int id)
    {
        var width = ResourceManager.Current.GetDesignGenerate().Width;
        return new Vector2Int(id % width, Mathf.FloorToInt(id / width));
    }
    public static int ConvertCoordinateToIdCandy(this Vector2Int coordinate)
    {
        if (spiralCoordinateList == null)
        {
            spiralCoordinateList = ResourceManager.Current.GetListSpiralCoordinate();
        }
        return spiralCoordinateList.FindIndex((v) => v == coordinate);
    }

    public static int ConvertBoardIdToIdCandy(this int _boardId)
    {
        var coordinate = _boardId.ConvertBoardIdToCoordinate();
        if (spiralCoordinateList == null)
        {
            spiralCoordinateList = ResourceManager.Current.GetListSpiralCoordinate();
        }
        return spiralCoordinateList.FindIndex((v) => v == coordinate);
    }
}
