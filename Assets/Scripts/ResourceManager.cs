using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum TypeOfTrajectory
{
    UP_RIGHT_SPIRAL,
    IN_OUT_SPIRAL,
    LEFT_DOWN_SPIRAL,
    RIGHT_DOWN_SPIRAL,
}

[Serializable]
public class TrajectoryResource
{
    public TypeOfTrajectory Type;
    public Trajectory Trajectory;
}

[Serializable]
public class ScoreResource
{
    public List<int> CandyId;
    public ScoreTable ScoreTable;
}

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Current;
    public SpriteSO CandySpriteData;
    public TypeOfTrajectory KindOfTrajectory;
    public List<TrajectoryResource> TrajectoryResource;
    public List<ScoreResource> ScoreResource;
    public DesignGenerate DesignGenerate;

    private void Awake()
    {
        if(Current == null)
        {
            Current = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public Sprite GetCandySprite(int Id)
    {
        if(CandySpriteData != null)
        {
            return CandySpriteData.GetSprite(Id);
        }
        else
        {
            Debug.LogError("SpriteData Resource is null !!!");
        }
        return null;
    }

    /// <summary>
    /// Get Passage Way of candy
    /// </summary>
    public List<Vector2> GetPassage()
    {
       var width = DesignGenerate.Width;
       var height = DesignGenerate.Height;

        if (TrajectoryResource != null)
        {
            var trajecRes = TrajectoryResource.FirstOrDefault((v) => v.Type == KindOfTrajectory);
            if(trajecRes != null)
            {
                return trajecRes.Trajectory.GetPassage(width, height);
            }
            Debug.LogError($"{KindOfTrajectory} is not exit !!!");
        }
        else
        {
            Debug.LogError("Trajectory is null !!!");
        }
        return null;
    }
    public List<Vector2Int> GetListSpiralCoordinate()
    {
        var width = DesignGenerate.Width;
        var height = DesignGenerate.Height;

        if (TrajectoryResource != null)
        {
            var trajecRes = TrajectoryResource.FirstOrDefault((v) => v.Type == KindOfTrajectory);
            if (trajecRes != null)
            {
                return trajecRes.Trajectory.GetListSpiralCoordinate(width, height);
            }
            Debug.LogError($"{KindOfTrajectory} is not exit !!!");
        }
        else
        {
            Debug.LogError("Trajectory is null !!!");
        }
        return null;
    }

    public DesignGenerate GetDesignGenerate()
    {
        if(DesignGenerate != null)
        {
            return DesignGenerate;
        }
        else
        {
            Debug.LogError("DesignGenerate is null !!!");
        }
        return null;
    }

    public int GetScore(int candyId, int amount)
    {
        var scoreTable = ScoreResource.FirstOrDefault((v) => v.CandyId.Contains(candyId));
        if(scoreTable != null)
        {
            return scoreTable.ScoreTable.GetScore(amount);
        }
        Debug.LogError($"ScoreResource is not contain candyId:: {candyId}");
        return 0;
    }
}
