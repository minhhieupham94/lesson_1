using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotData : IData
{
    public int Id;
    public Vector2Int Coordinate;

    public SlotData(int _id, Vector2Int _coor)
    {
        Id = _id;
        Coordinate = _coor;
    }
    public void OnDispose()
    {
    }
}

public class Slot : MonoBehaviour
{
    public Button Button;
    private SlotData data;

    public void Init(IData _data)
    {
       if(_data is not null)
       {
            data = _data as SlotData;
       }
    }

    public void OnEnableButton(bool val)
    {
        if(Button.interactable != val)
        {
            Button.interactable = val;
        }
    }

    public void OnSelected()
    {
        OnSelectedAction?.Invoke(data);
    }

    private Action<SlotData> OnSelectedAction;

    public void OnSelectedRegister(Action<SlotData> act)
    {
        if(act is not null)
        {
            OnSelectedAction = act;
        }
    }
    public void OnSelectedUnRegister(Action<SlotData> act)
    {
        if (OnSelectedAction is not null)
        {
            OnSelectedAction -= act;
        }
    }
}
