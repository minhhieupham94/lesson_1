using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CandyData:IData
{
    public int Id;
    public float Velocity;
    public List<Vector2> Passage;
    public bool Empty;
    public int Type = -1;

    public void SetId(int _id)
    {
        if(Id != _id)
        {
            Id = _id;
        }
    }

    public int GetId() => Id;

    public void OnDispose()
    {
    }
}

public class Candy : MonoBehaviour
{
    public float HideVelocity;
    private Image icon;
    private CandyData data;
    public bool show;
    public int spiralIndx;
    private float time;
    private float distanceDelta;
    private Action onShowComplete;
    public bool hide;
    private float hideSpeed;
    private Action onHideComplete;
    private bool isDestroy;
    public bool swap;
    private Action onSwapComplete;
    private Vector2 targetSwap;

    public void Initialized(IData _data)
    {
        SetData(_data);
        show = false;
        icon = GetComponent<Image>();
        SetIcon(data.Type);
        isDestroy = false;
    }

    public void SetData(IData _data)
    {
        if (_data != null)
        {
            data = _data as CandyData;
        }
    }

    public void SetSpiral(int val)
    {
        spiralIndx = val;
    }

    public int GetSpiral() => spiralIndx;

    private void SetIcon(int _type)
    {
       if(icon)
       {
            icon.sprite = ResourceManager.Current.GetCandySprite(_type);
       }
    }

    public void DoShow(Action _onShowComplete)
    {
        isDestroy = false;
        show = true;
        if(spiralIndx > data.Id)
        {
            spiralIndx = data.Id;
        }
        onShowComplete = _onShowComplete;
    }
    public void DoHide(Action _onHideCompele)
    {
        isDestroy = true;
        hide = true;
        hideSpeed = 0;
        onHideComplete = _onHideCompele;
    }
    public void DoSwap(Vector2 target, Action _onSawpComplete)
    {
        swap = true;
        onSwapComplete = _onSawpComplete;
        targetSwap = target;
    }
    public void MoveToTarget(Vector2 from, Vector2 to, float distanceDelta)
    {
        var pos = Vector2.Lerp(from, to, distanceDelta);
        transform.position = pos;
    }

    public void MainFixedUpdate(float fixedDetaTime)
    {
        if (show)
        {
            ShowAnimation(fixedDetaTime);
        }

        if(hide)
        {
            HideAnimation(fixedDetaTime);
        }

        if(swap)
        {
            DoSwapAnimation(fixedDetaTime);
        }
    }

    private void DoSwapAnimation(float fixedDetaTime)
    {
        if (distanceDelta < 1)
        {
            time += fixedDetaTime;
            distanceDelta = time / data.Velocity;

            MoveToTarget(transform.position, targetSwap, distanceDelta);
        }
        else
        {
            swap = false;
            time = 0;
            distanceDelta = 0;
            onSwapComplete?.Invoke();
            onSwapComplete = null;
        }
    }

    private void ShowAnimation(float fixedDetaTime)
    {
        if (spiralIndx == 0 && time == 0)
        {
            gameObject.SetSafeActive(true);
            SetAlpha(ref icon,1);
            SetScale(transform, 1);
        }

        if (spiralIndx < data.Id)
        {
            if (distanceDelta < 1)
            {
                time += fixedDetaTime;
                distanceDelta = time / data.Velocity;

                MoveToTarget(data.Passage[spiralIndx], data.Passage[spiralIndx + 1], distanceDelta);
            }
            else
            {
                //  Debug.Log($"time {time}");
                time = 0;
                spiralIndx++;
                distanceDelta = 0;
            }
        }

        if (spiralIndx > data.Id)
        {
            spiralIndx = data.Id;
            transform.position = data.Passage[data.Id];
        }

        if (spiralIndx == data.Id)
        {
            transform.position = data.Passage[data.Id];
            onShowComplete?.Invoke();
            show = false;
        }
    }
    private void HideAnimation(float fixedDetaTime)
    {
        hideSpeed += HideVelocity * fixedDetaTime;
        var val = Mathf.Lerp(1, 0, hideSpeed);
        SetAlpha(ref icon, val);
        SetScale(transform, val);
        if(hideSpeed >= 1)
        {
            hide = false;
            onHideComplete?.Invoke();
        }
        Debug.Log($"HideAnimation {hideSpeed}");
    }
    private void SetAlpha(ref Image _image, float _val)
    {
        var temp = _image.color;
        _val = Math.Clamp(_val, 0, 1);
        if(temp.a != _val)
        {
            temp.a = _val;
            _image.color = temp;
        }
    }
    private void SetScale(Transform _trf, float _val)
    {
        _val = Mathf.Clamp(_val, 0, 1);
        _trf.localScale = Vector3.one * _val;
    }
    public int GetType()
    {
        if (data != null)
            return data.Type;
        return -1;
    }
    public CandyData GetData() => data;
    public bool IsEmty => data.Empty;
    public void SetEmpty()
    {
        data.Empty = true;
    }
    public bool IsDestroy => isDestroy;
    private void OnDestroy()
    {
        onHideComplete = null;
        onShowComplete = null;
    }

}
